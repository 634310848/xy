module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    // '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'indent': 'off',
    "space-before-function-paren": 0,
    // 'quotes': [2, 'single', {
    //   'avoidEscape': true,
    //   'allowTemplateLiterals': true
    // }], //引号类型 `` "" ''
  }
}
