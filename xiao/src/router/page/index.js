import Home from '../../views/Home.vue'

export default [
  {
    path: '/login',
    name: '登录页',
    component: () =>
      import(/* webpackChunkName: "page" */ '@/pages/login/login'),
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false
    }
  },
  {
    path: '/home',
    name: '首页',
    component: () =>
      import(/* webpackChunkName: "page" */ '@/pages/home/home'),
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false
    }
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  }
]
