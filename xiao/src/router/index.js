import Vue from 'vue'
import VueRouter from 'vue-router'
import pageRouter from './page/'
Vue.use(VueRouter)

const routes = pageRouter
const router = new VueRouter({
  routes
})

export default router
