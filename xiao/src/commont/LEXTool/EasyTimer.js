import {EasyCode} from './easyCode';

export default class EasyTimer{

    time =60;
    isBegin =false;
    finishCallback =null;
    secCallback =null;
    keepFirstTime =0;

    start(time, secCallback, finishCallback){
        if (time===''){return;}
        this.isBegin =true;
        this .time =time;
        this .keepFirstTime =time;
        if (EasyCode.IsNotEmpty(secCallback)){
            this.secCallback =secCallback;
        }
        if (EasyCode.IsNotEmpty(finishCallback)){
            this.finishCallback =finishCallback;
        }
        this.counterBegin();
    }

    counterBegin(){
        if (this.time<=0){
            this.isBegin =false;
            if (this.finishCallback ===null){return;}
            this.finishCallback(this.time);
            return;
        }
        this.time -=1;
        clearTimeout();
        setTimeout(()=>{
            if (this.secCallback ===null){return;}
            this.secCallback( this.time);
            this.counterBegin();
        },1000);
    }

    getTime(){
        return this.time;
    }

    setTime(time){
        this.time =time;
    }

    //执行毫秒计数
    cacStop =false;
    endTime =0;
    text ='';
    cacTime(time){
        if (this.cacStop){
            this.endTime =0;
            console.warn(this.text +':' +time +'ms');
            return;
        }
        setTimeout( ()=>{
            time +=1;
            this.cacTime(time);
        }, 1);
    }

    stopCac(text=''){
        this.text =text;
        this.cacStop =true;
    }





}
