export let YearType={
    ruannian:'ruannian'  ,
    pingnian:'pingnian'
};
export let EasyDate={
    yearType(year =new Date().getFullYear()){
        let isRuannian =
            parseInt(year)%4===0
            &&parseInt(year)%100!==0
        ;
        return isRuannian?YearType.ruannian :YearType.pingnian;
    },

    dayTotalOfYear(year =new Date().getFullYear()){
        let isRuannian =
            parseInt(year)%4===0
            &&parseInt(year)%100!==0
        ;
        return isRuannian?366:365;
    },




    getDateOfDayBefore(day){
        var date = new Date();
        date.setDate(date.getDate() + parseInt(day));
        return date.getFullYear() +'-'+ (date.getMonth()+1) +'-'+ date.getDate();
    },

    getMinOfDayBefore(min){
        var date = new Date();
        date.setMinutes(date.getMinutes() + parseInt(min));
        return date.getFullYear() +'-'+ (date.getMonth()+1) +'-'+ date.getDate() +'  '
            +date.getHours() +':' +date.getMinutes()
            ;
    },


    /*字符转日期*/
    StringToDate(DateStr){
        if(typeof DateStr ==='undefined')
            return new Date();
        if(typeof DateStr ==='date')return DateStr;
        var converted = Date.parse(DateStr);
        var myDate = new Date(converted);
        if(isNaN(myDate)){
            DateStr=DateStr.replace(/:/g,"-");DateStr=DateStr.replace(" ","-");DateStr=DateStr.replace(".","-");
            var arys= DateStr.split('-');
            switch(arys.length){
                case 7 :
                    myDate = new Date(arys[0],--arys[1],arys[2],arys[3],arys[4],arys[5],arys[6]);
                    break;
                    case 6 : myDate = new Date(arys[0],--arys[1],arys[2],arys[3],arys[4],arys[5]);
                    break;
                    default: myDate = new Date(arys[0],--arys[1],arys[2]);
                    break;
            }
        }
        return myDate;
    },

    // 字符串时间相减
    TimeStrSubtract(dateStrOne,dateStrTow){

        var timestampA = new Date(this.StringToDate(dateStrOne)).getTime();
        var timestampB = new Date().getTime();
        if (dateStrTow) {
            timestampB = new Date(this.StringToDate(dateStrTow)).getTime();
        }else {
            timestampB = new Date().getTime();
        }
        if (timestampA>timestampB){
            let timestamp =  timestampA - timestampB;
            return timestamp
        } else{
            let timestamp =  timestampB - timestampA;
            return timestamp
        }

    },

    timestampToHMS(timestamp,toStr){

        var hours = parseInt( timestamp / (1000 * 60 * 60));
        hours<10?hours='0' + hours:hours;
        var minutes = parseInt((timestamp % (1000 * 60 * 60)) / (1000 * 60));
        minutes<10?minutes='0' + minutes:minutes;
        var seconds = parseInt((timestamp % (1000 * 60)) / 1000);
        seconds<10?seconds='0' + seconds:seconds;
        var newTime=  hours + " : " + minutes + " : " + seconds + "  ";
        if (toStr) {
            return newTime
        }else {
            return {
                hours:hours,
                minutes:minutes,
                seconds:seconds
            }
        }
    },


    getNowTime(){
        let date =new Date();
      return   date.getHours() +':' +date.getMinutes();
    }



};
