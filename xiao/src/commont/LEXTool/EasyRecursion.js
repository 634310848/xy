import {EasyCode} from "./easyCode";

export default class EasyRecursion{
    startTime =0;
    stepCall =()=>{};
    endTime =10;
    stepMs =1;

    init(startTime, stepMs,  endTime, stepCall,){
        this.startTime =startTime;
        this.stepCall =stepCall;
        this.endTime =endTime;
        this.step(startTime);
    }


    step(time){
        console.warn(time, this.stepMs)
        if (time >=this.endTime){
            this.stepCall(time);
            return;
        } else{
            setTimeout(()=>{
                this.step(time +this.stepMs);
            },100);

        }
    }



}

