import {Platform ,BackHandler} from "react-native";
import {Linking, Clipboard} from "react-native";


export const EasyDevice={

    //获取日期
    getDate:function() {
        var date = new Date();

        var year = date.getFullYear().toString();
        var month = (date.getMonth()+1).toString();
        var day = date.getDate().toString();

        return year+'-'+month+'-'+day;
    },

    //获取日期
    getYear:function() {
        var date = new Date();

        var year = date.getFullYear().toString();


        return year;
    },

    getMon:function() {
        var date = new Date();

        var month = (date.getMonth()+1).toString();

        return month;
    },

    //获取时间
    getTime:function() {
        var date = new Date();

        var hour =  date.getHours().toString();
        var minute = date.getMinutes().toString();

        return hour+':'+minute;
    },

    //物理键生成后退操作
    goBack() {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener("back", ()=>{
                this.props.navigation.goBack();
            });
        }
    },



    // 拨打电话
    callPhone(phone) {
        let url = "tel:" + phone;
        Linking.canOpenURL(url).then(supported => {
            if (!supported) {
                console.warn('Can\'t handle url: ' + url);
            } else {
                return Linking.openURL(url);
            }
        }).catch(err => (console.error('An error occurred', err)));

    },

    // 复制到粘贴板
    async copyTextToClipboard(text, successCallback, faildCallBack) {
        Clipboard.setString(text);
        try {
            var content = await Clipboard.getString();
            if (content === text) {
                successCallback(text);
            }
            else {
                faildCallBack()
            }
        } catch (e) {
            console.log(e)
            faildCallBack(e)
        }
    }




};